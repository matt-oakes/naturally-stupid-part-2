*************************************************
*	               IMPORTANT                    *
*************************************************

Use "import comp34120.ex2.*" in the Leader
This solves all problems when you use types that are required form the precompiled class files.


*************************************************
*	CALLABLE THINGS IN THE PLATFORM	            *
*************************************************
	3.2.1. Checking the status of the RMI connection
		public void checkConnection(final PlayerType p_type)
			throws RemoteException;

	It simply does nothing. If the connection fails, the JVM will throw a RemoteException, so that you know the failure of the connection. Normally nothing will be done by this method. You may not need this method. The platform provides this just in case you want to make your leader program more robust.

	3.2.3. Get the information of price and cost

	public Record query(final PlayerType p_type,
		final int p_queryDate)
		throws RemoteException;

	By calling this method, you can get the price of both the leader and the follower as well as the cost of a given day. The Record is a public class which contains the price and cost in it. This is a very important method you may want to use in your own leader.

	3.2.2. Registering to the platform

	public void registerPlayer(final PlayerType p_type,
		final String p_displayName)
		throws RemoteException, NotBoundException;

	Register this leader instance to the platform. Normally you will not need to call this method because RMI registeration procedure has already been implemented in the abstract class PlayerImpl which you need to extend to implement your own leader. This method will be called automatically in the constructor of the PlayerImpl which you need to call in the first line of the constructor of your own leader class.

	3.2.4. Publish your new price

	public void publishPrice(final PlayerType p_type,
		final float p_price)
		throws RemoteException;

	Publish your new price by calling this method. After calculating your profit and get a optimal price, you need to call this method to finish your phase of this simulation day.

	3.2.5. Output some information to the GUI

	public void log(final PlayerType p_type,
		final String p_text)
		throws RemoteException;

	By calling this method you can publish some information (e.g. debug information) on the GUI.

*************************************************
*	IMPLEMENTABLE THINGS IN THE LEADER          *
*************************************************

	3.3.1. Check the status of the RMI connection

	public void checkConnection()
		throws RemoteException;

	Similar to the method of the platform, this method does nothing, just for detection of connection failure. You do not need to override this method.

	3.3.2. Inform the end of the program

	public void goodbye()
		throws RemoteException;

	The platform will call this method when you close the GUI of the platform. An example of the use of this method can be found in SimpleLeader, after calling which the program exits. You may make the choice whether override this method or not.

	3.3.3. Start the simulation

	public void startSimulation(final int p_steps)
		throws RemoteException;

	The platform will call this method when the simulation begins. You may want to do some initialization here, otherwise you don't need to override it.

	3.3.4. End the simulation

	public void endSimulation()
		throws RemoteException;

	The platform will call this method when the simulation ends. You may want to do some finalization here, otherwise you don't need to override it.

	3.3.5. Proceed to a new day

	public void proceedNewDay(final int p_date)
		throws RemoteException;

	Platform will call this method when the simulation proceeds to a new day. This is the only method you *MUST* implement. Here you may want to ask the platform of the price and cost of the day before the current day. You may also put all your math stuff here to compute an optimal price. Your phase will end after your done everything here.


