package comp34120.ex2;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import static java.lang.System.out;

final class BossLeader extends PlayerImpl {
    /**
     * Setting to true enabled the debug logs
     **/
    private static final boolean DEBUG = true;
    /**
     * The cost of the product
     **/
    private static final float COST = 1.0f;
    
    private int numberOfDays;

    private int dayWeAreOn = 60;

    // Boring setup stuff
    BossLeader() throws RemoteException, NotBoundException {
        super(PlayerType.LEADER, "HueHueHue");
    }
    public static void main(final String[] p_args) throws RemoteException, NotBoundException {
        new BossLeader();
    }
    private void log(String msg) throws RemoteException {
        if (DEBUG) {
            m_platformStub.log(PlayerType.LEADER, msg);
        }
    }
    // Done with the boring stuff now

    @Override
    public void startSimulation(final int p_steps) throws RemoteException {
        super.startSimulation(p_steps);
        numberOfDays = p_steps;
    }

    @Override
    public void endSimulation() throws RemoteException {
        List<Record> records = getInfo(61 + numberOfDays, numberOfDays);

        float totalProfit = 0.0f;
        for (Record record : records) {
            float leaderPrice = record.m_leaderPrice;
            float followerPrice = record.m_followerPrice;
            float profit = (float) ((leaderPrice - COST) * (2 - leaderPrice + (0.3 * followerPrice)));
            totalProfit += profit;
        }

        System.out.println("Profit: " + totalProfit);
    }

    /**
     * To inform this instance to proceed to a new simulation day
     * @param date The date of the new day
     * @throws RemoteException This exception *MUST* be thrown by this method
     */
    @Override
    public void proceedNewDay(int date) throws RemoteException {    
        dayWeAreOn = date;    
        // Perform linear regression on data
        RegressionResults regressionResults = linearRegression(date);

        // Calculate the constants used in the derived equations
        float constant1 = -1.0f + (0.3f * regressionResults.m);
        float constant2 = 0.3f * regressionResults.c;

        // Check the 2nd derivation is less than 0 so we know it's a maximum
        float check = 2.0f * constant1;
        if (check < 0) {
            // Calculate the best leader price using the 1st derivation
            float leaderPrice = ((constant1 * COST) - constant2 - 2.0f) / (2.0f * constant1);
            // Work out our profile for for the hell of it
            float leaderProfit = (leaderPrice - COST) * (2.0f + (constant1 * leaderPrice) + constant2);
            // Publish the price we worked out was the best
            m_platformStub.publishPrice(PlayerType.LEADER, leaderPrice);
        }
        else {
            // TODO: This should never happen but we sould publish a price here
        }
    }


    /**
     * Get data from day 1 to maxDay
     * @param maxDay The day we want to be the last record
     * @param numberOfRecords The number of records to return
     * @return The list of records asked for
     * @throws RemoteException This exception *MUST* be thrown by this method
     */
    private List<Record> getInfo(int maxDay, int numberOfRecords) throws RemoteException {
        List<Record> records = new ArrayList<Record>();

        // Figure out the start day, starting at least at 1
        int startDay = Math.max(maxDay - numberOfRecords + 1, 1);

        // log("Start: " + startDay + " Max:" + maxDay);

        for (int i = startDay; i <= maxDay; i++) {
            Record record = m_platformStub.query(PlayerType.LEADER, i);
            records.add(record);
        }

        return records;
    }

    private RegressionResults linearRegression(int date) throws RemoteException {
        int bestWindowSize = -1;
        float bestError = 100000.0f;
        for (int windowSize = 5; windowSize <= 30; windowSize++) {
            float err  = training(windowSize);

            if (err < bestError) {
                bestWindowSize = windowSize;
                bestError = err;
            }
        }
        //System.out.println("Best Error: " + bestError);
        //System.out.println("Best Window size: " + bestWindowSize);

        List<Record> records = getInfo(date, bestWindowSize);
        RegressionResults results = ordinaryLeastSquare(records);
        
        return results;
    }

    private float training(int days) throws RemoteException { 
        
        // train for 'days' days
        // do not predicted more than one day ahead without re-training
        List<Record> training = getInfo(days,days);
        List<Record> testing = getInfo(dayWeAreOn ,dayWeAreOn-days);
        List<Record> predicted = new ArrayList<Record>();
        RegressionResults res = ordinaryLeastSquare(training);
        int currentDay = 1;
        for (int i = 0; i < testing.size(); i++) {
            float followerPrice = (res.m * testing.get(i).m_leaderPrice) + res.c;
            Record t = new Record(currentDay, 0.0f, followerPrice, 1.0f);
            predicted.add(t);
            currentDay++;
        }
        float error = rootMeanSquareError(testing,predicted);
        return error;
    }

   

    /*
    *
    *
    *
    */
    private RegressionResults thielSen(List<Record> records) throws RemoteException {
        int num = records.size();
        double[] X_ARRAY = new double[records.size()];
        double[] Y_ARRAY = new double[records.size()];
        Record record;
        for (int i = 0;i < num;i++) {
            record = records.get(i);
            X_ARRAY[i] = record.m_leaderPrice;
            Y_ARRAY[i] = record.m_followerPrice;    
         }
        int nlimit = (int) (num*(num-1)/2);

        double[] slopes = new double[nlimit];
        double[] intercepts=new double[num];
        double x1=0.0, y1=0.0;
        int cnt=0;

        ///////do thiel-sen
        for(int i=0;i<(num-1);i++)
        {
            x1=X_ARRAY[i];y1=Y_ARRAY[i];
            for(int j=i+1;j<num;j++)
            {
                slopes[cnt] = (double) ((Y_ARRAY[j]-y1)/(X_ARRAY[j]-x1));
                cnt++;
            }
        }

        double m = median(slopes,num);///this is 'm' in regression eqn

        for(int k=0;k<(num-1);k++)
        {
        intercepts[k]=Y_ARRAY[k]-(m*X_ARRAY[k]);
        //System.out.println(intercepts[k]);
        }

        double intercept = median(intercepts,num);///this is 'b' in regression eqn
        double c = intercept*(median(intercepts,num));

        RegressionResults result = new RegressionResults((float)m,(float)c);
        return result;  
    }

    public double median(double[] d, int num)
    {
        double[] newd = dSort(d,num);
        int nhalf = (int) (num/2);
        if(num%2!=0){return(newd[nhalf]);}else{return((double)((newd[nhalf-1]+newd[nhalf])/2));}
    }

    public double[] dSort(double dArray[],int num)
    {
        for(int i=0;i<num-1;i++)
            for(int j=i+1;j<num;j++)
            //start bubblesort
        if(dArray[i] > dArray[j])
        {
        double dTemp = dArray[i];
        dArray[i] = dArray[j];
        dArray[j] = dTemp;
        }
        return(dArray);
    }

    /*
    *
    *
    *
    */
    private RegressionResults lmsRegression(List<Record> records) throws RemoteException{
        double sumLeader = 0.0, sumReaction = 0.0, noDatas = 0.0, sumLeaderXReaction = 0.0;
        double aHat = 0.0, bHat = 0.0;
        noDatas = records.size();
        Record record;
        for (int i = 0;i < records.size(); i++) {
            record = records.get(i);
            sumLeader += record.m_leaderPrice;
            sumReaction += record.m_followerPrice;
            sumLeaderXReaction += (record.m_leaderPrice * record.m_followerPrice);
        }
        // a hat calculation

        aHat = ((sumLeader * sumReaction) - (sumLeader * sumLeaderXReaction))/ ((noDatas*sumLeader)-(Math.pow(sumLeader,2)));

        //b hat calculation
        bHat = ((noDatas*sumLeaderXReaction)-(sumLeader * sumReaction)) / ((noDatas * sumLeader)-(Math.pow(sumLeader,2)));

        RegressionResults result  = new RegressionResults((float)bHat,(float)aHat);
        return result;
    }

    /**
     * Perform linear regression on the given records
     * @param records The records which contain leader and follower price at each day
     * @return The results from the linear regression
     **/
    private RegressionResults ordinaryLeastSquare(List<Record> records) throws RemoteException{
        int MAXN = 1000;
        int numRecords = records.size();
        float[] x = new float[MAXN];
        float[] y = new float[MAXN];

        // First pass: read in data, compute xbar and ybar
        float sumx = 0.0f, sumy = 0.0f, sumx2 = 0.0f;
        Record record;
        for (int i = 0; i < numRecords; i++) {
            record = records.get(i);
            x[i] = record.m_leaderPrice;
            y[i] = record.m_followerPrice;
            sumx  += x[i];
            sumx2 += x[i] * x[i];
            sumy  += y[i];
        }
        float xbar = sumx / numRecords;
        float ybar = sumy / numRecords;

        // Second pass: compute summary statistics
        float xxbar = 0.0f, yybar = 0.0f, xybar = 0.0f;
        for (int i = 0; i < numRecords; i++) {
            xxbar += (x[i] - xbar) * (x[i] - xbar);
            yybar += (y[i] - ybar) * (y[i] - ybar);
            xybar += (x[i] - xbar) * (y[i] - ybar);
        }

        // Calculate the m and c
        float m = xybar / xxbar;
        float c = ybar - m * xbar;

        // Put the results into the object ready to return
        RegressionResults results = new RegressionResults(m, c);
        return results;
    }

    /**
     * Calculates and returns the Root Mean Square Error. Useful for getting the absolute
     * error when comparing regression methods and window sizes.
     * @param records The historic data
     * @param regressionResults The m and c values calculated by the linear regression
     * @return The RMSE
     **/
    private float rootMeanSquareError(List<Record> actualData, List<Record> predictedData) {
        // Get the sum of the squared difference
        float differenceSquaredTotal = 0.0f;
        for (int t = 0; t < actualData.size(); t++) {
            // Get the historic data at time t
            Record followerActual = actualData.get(t);
            Record followerPredicted = predictedData.get(t);
            // Get the difference, square it an add to total
            float difference = followerActual.m_followerPrice - followerPredicted.m_followerPrice;
            differenceSquaredTotal += Math.pow(difference, 2);
        }

        // Get the average squared difference
        float averageDifferenceSquared = differenceSquaredTotal / actualData.size();
        // Return the root of this - the RMSE
        return (float) Math.sqrt(averageDifferenceSquared);
    }

    /**
     * Calculates and returns the Mean Absolute Percentage Error.
     * @param records The historic data
     * @param regressionResults The m and c values calculated by the linear regression
     * @return The MAPE
     **/
    private float meanAbsolutePercentageError(List<Record> records, RegressionResults regressionResults) {
        // Get the sum of the stuff
        float stuffTotal = 0.0f;
        for (int t = 0; t < records.size(); t++) {
            // Get the historic data at time t
            Record record = records.get(t);
            // Get the estimate of the followers price using m, c and leaders price (x)
            // mx + c
            float followerPriceEstimate = (regressionResults.m * record.m_leaderPrice) + regressionResults.c;
            // Add the stuff to the total
            float stuff = (record.m_followerPrice - followerPriceEstimate) / record.m_followerPrice;
            stuffTotal += Math.abs(stuff);
        }

        // Return the average
        return (stuffTotal / records.size()) * 100;
    }

    /**
     * Stores the results from the linear regression calculation
     **/
    private class RegressionResults {
        public float m;
        public float c;

        public RegressionResults(float m, float c) {
            this.m = m;
            this.c = c;
        }

        public String toString() {
            return "(m:" + m + " c:" + c + ")";
        }
    }
}
